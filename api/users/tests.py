from django.test import TestCase
from .models import CustomUser
from rest_framework.test import APIRequestFactory
from django.test import Client


class TestCustomUserModel(TestCase):
    def setUp(self):
        CustomUser.objects.create(username="lion")

    def test_user_can_fetch_from_db(self):
        lion = CustomUser.objects.get(username="lion")
        lion.generate_token()
        self.assertEqual(lion.username, "lion")
        self.assertNotEqual(lion.token, "")


class TestAPIURL(TestCase):
    def setUp(serf):
        pass

    def test_can_access_login_url(self):
        c = Client()
        response = c.post(
            "/api/users/login",
            {
                "user": {
                    "username": "Jacob",
                    "email": "jake@jake.jake",
                    "password": "jakejake",
                }
            },
        )
        self.assertEqual(response.status_code, 200)


# class TestCustomerUserModelAPI(TestCase):
#     def setUp(self) -> None:
#         factory = APIRequestFactory()
#     def test_user_api(self):
#         request = self.factory.get('/users/' )
#         self.assertEqual(request, "")

# Create your tests here.
