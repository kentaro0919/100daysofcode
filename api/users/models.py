import pendulum
import jwt
from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django.db import models
from django_extensions.db.models import CreationDateTimeField, ModificationDateTimeField


class CustomUser(AbstractUser):
    """
    "user": {
    "email": "jake@jake.jake",
    "token": "jwt.token.here",
    "username": "jake",
    "bio": "I work at statefarm",
    "image": null
    """

    bio = models.TextField(blank=True)
    token = models.TextField(blank=True)
    image = models.URLField(blank=True)
    created_at = CreationDateTimeField()
    modified_at = ModificationDateTimeField()

    def generate_token(self):
        payload = {
            "issued": str(pendulum.now()),
            "expires": str(pendulum.now().add(minutes=1)),
            "refresh": str(pendulum.now().add(days=1)),
            "scopes": "",
        }
        self.token = bytes(
            jwt.encode(payload, settings.JWT_KEY, algorithm="HS256")
        ).decode("utf-8")
        self.save()
        return self

    def make_blog(self, blog_name: str):
        from blog.models import Blog

        return Blog.objects.get_or_create(author=self, blog_name=blog_name)[0]

    def get_blog(self, blog_name: str):
        from blog.models import Blog

        return Blog.objects.get(author=self, blog_name=blog_name)

    def get_blgos(self):

        return self.blog_set

    def get_all_articles(self):
        blogs = self.get_blgos()
        return [blog.get_articles() for blog in blogs.all()]
