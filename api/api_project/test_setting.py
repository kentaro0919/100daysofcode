from .settings import *

SECRET_KEY = "test"

DATABASES = {"default": {"ENGINE": "django.db.backends.sqlite3", "NAME": "mydatabase"}}
