from django.test import TestCase
from .models import Tag, Blog
from users.models import CustomUser


class TestTagModel(TestCase):
    def setUp(self) -> None:
        Tag.objects.create(tag="new tag")

    def test_tag_can_fetch_frmo_db(self):
        tag = Tag.objects.get(tag="new tag")
        self.assertEqual(tag.tag, "new tag")


class TestBlogModel(TestCase):
    def setUp(self) -> None:
        tag = Tag.objects.create(tag="new tag")
        user1 = CustomUser.objects.create(username="lion")
        user1.make_blog(blog_name="My First Blog")
        firts_blog = user1.get_blog("My First Blog").make_article(
            title="First titel",
            slag="first_slag",
            description="description First article",
            body="body first article",
        )
        firts_blog.article_set.all().first().tag_list.add(tag)

    def test_user_can_make_a_blog(self):
        user1 = CustomUser.objects.get(username="lion")
        blogs = user1.get_blgos().first()

        self.assertEqual(blogs.blog_name, "My First Blog")
        self.assertEqual(blogs, user1.get_blog("My First Blog"))

    def test_user_can_post_a_page_to_firts_blog(self):
        first_tag = Tag.objects.get(tag="new tag")
        user1 = CustomUser.objects.get(username="lion")
        articles = user1.get_all_articles()

        self.assertEqual(articles[0][0].title, "First titel")
        self.assertEqual(articles[0][0].tag_list.first().tag, first_tag.tag)
