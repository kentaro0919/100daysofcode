from django.db import models
from django_extensions.db.models import CreationDateTimeField, ModificationDateTimeField


class Tag(models.Model):
    tag = models.SlugField(db_index=True, unique=True)
    created_at = CreationDateTimeField()
    modified_at = ModificationDateTimeField()


class Blog(models.Model):
    from users.models import CustomUser

    blog_name = models.CharField(max_length=255)
    author = models.ForeignKey(CustomUser, on_delete=models.CASCADE)

    def get_articles(self):
        return self.article_set.all()

    def make_article(self, title, slag, description, body, tag_list=None):
        article = Article.objects.create(
            article_of=self, title=title, slag=slag, description=description, body=body
        )
        if tag_list:
            [
                article.tag_list.add(Tag.objects.get_or_create(tag=tag)[0])
                for tag in tag_list
            ]
        return self


class Article(models.Model):
    """
  "article":
    "title": "How to train your dragon",
    "description": "Ever wonder how?",
    "body": "You have to believe",
    "tagList": ["reactjs", "angularjs", "dragons"]
    """

    article_of = models.ForeignKey(Blog, on_delete=models.CASCADE)
    title = models.CharField(max_length=255)
    slag = models.SlugField()
    description = models.TextField()
    body = models.TextField()
    tag_list = models.ManyToManyField(Tag, related_name="tagList")
