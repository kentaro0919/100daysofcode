# About Git

[git mental model](https://dsinecos.github.io/blog/Git-basics)

## git-add - Add file contents to the index

```
$ git add {file name}
```

## git-commit - Record changes to the repository

```
$ git commit -m "add README"
```

## git-push - Update remote refs along with associated objects

### -u, --set-upstream

```
$ git push -u origin master
```

## git-merge - Join two or more development histories together

```
$ git merge {branch name}
```

[git flow](https://github.com/petervanderdoes/gitflow-avh/wiki/Reference:-git-flow-init)  
[git flow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow)

# About Vim

[spacevim](https://spacevim.org/documentation/#core-pillars)
[vim](https://bencrowder.net/files/vim-fu/#insert)

I -> Jump to beginning of the current line and enter insert mode.

# About Gitlab

[ci/cd](https://docs.gitlab.com/ee/ci/examples/test-and-deploy-python-application-to-heroku.html)

# About Django

[django](https://docs.docker.com/compose/django/)
[Testing in Django](https://docs.djangoproject.com/en/2.2/topics/testing/)
[django-extensions](https://django-extensions.readthedocs.io/en/latest/installation_instructions.html)

### check then change in model can run without effects

```shell script
$ docker-compose run web ./manage.py makemigrations users --dry-run

```

### Display list of fields

```python
>>> from users.models import CustomUser
>>> CustomUser._meta.fields
(<django.db.models.fields.AutoField: id>, <django.db.models.fields.CharField: password>, <django.db.models.fields.DateTimeField: last_login>, <django.db.models.fields.BooleanField: is_superuser>, <django.db.models.fields.CharField: username>, <django.db.models.fields.CharField: first_name>, <django.db.models.fields.CharField: last_name>, <django.db.models.fields.EmailField: email>, <django.db.models.fields.BooleanField: is_staff>, <django.db.models.fields.BooleanField: is_active>, <django.db.models.fields.DateTimeField: date_joined>)
>>> [print(fields) for fields in CustomUser._meta.fields]
users.CustomUser.id
users.CustomUser.password
users.CustomUser.last_login
users.CustomUser.is_superuser
users.CustomUser.username
users.CustomUser.first_name
users.CustomUser.last_name
users.CustomUser.email
users.CustomUser.is_staff
users.CustomUser.is_active
users.CustomUser.date_joined

```

# About Docker

## Docker

## Docker-compose

[docker-compose](https://docs.docker.com/compose/)
read variables from fle
[Environment variables in Compose](https://docs.docker.com/compose/environment-variables/)

```shell script
# run manager
$ docker-compose run web ./manage.py test -d -v 3

# run coverage
$ docker-compose run web coverage run --source='.' manage.py test$ docker-compose run web coverage run --source='.' manage.py test

# get coverage html
$ docker-compose run web coverage html$ docker-compose run web coverage html
$ docker cp `docker-compose ps -q web`:/code/htmlcov .

```

# About Heroku

[create app](https://devcenter.heroku.com/articles/creating-apps)
[Docker](https://devcenter.heroku.com/articles/build-docker-images-heroku-yml)

```
$ heroku create {app name}
$ heroku stack:set container -a {app name}
$ heroku container:login
$ heroku container:push web -a {app name}
```

# About shell

```
$ HAPP="{app name}"
$ $HAPP
```

[Automate Python workflow using pre-commits: black and flake8](https://ljvmiranda921.github.io/notebook/2018/06/21/precommits-using-black-and-flake8/)
