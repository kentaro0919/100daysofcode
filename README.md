# My New plan to become a better programmer.

## Try to use TDD as much as possible. Planning can be included for the hour.

### Use vim git juts in cli.

_[RealWorld](https://github.com/gothinkster/realworld)_

1. ~~Start 100 days of code~~ Done at 2019-08-24
2. ~~Setup a new repo in Gitlab~~ Done at 2019-08-24
3. ~~Setup Django~~ Done at 2019-08-24
4. ~~Setup CI~~ Done at 2019-08-24
5. ~~Setup Heroku as docker~~ Done at 2019-08-29
6. ~~Setup user model with tests.~~ Done at 2019-08-30
7. ~~Set up user Mode with test~~ Done at 2019-08-30
8. Set up blog Mode with test
9. Set up post Mode with test
10. ~~Set up tag Model with test~~ Done at 2019-08-30
11. ~~CRUD done just with minimal-Admin-page.~~ Done at 2019-09-01
12. ~~Add auto swagger documentation~~ Done at 2019-09-02
13. Authentication: POST /api/users/login
14. Registration: POST /api/users
15. Add React front
16. Get Current User GET /api/user
17. Add React front
18. Update User PUT /api/user
19. Add React front
20. Get Profile GET /api/profiles/:username
21. Add React front
22. Follow user POST /api/profiles/:username/follow
23. Add React front
24. Unfollow user DELETE /api/profiles/:username/follow
25. Add React front
26. List Articles GET /api/articles
27. Add React front
28. Feed Articles GET /api/articles/feed
29. Add React front
30. Get Article GET /api/articles/:slug
31. Add React front
32. Create Article POST /api/articles
33. Add React front
34. Update Article PUT /api/articles/:slug
35. Add React front
36. Delete Article DELETE /api/articles/:slug
37. Add React front
38. Add Comments to an Article POST /api/articles/:slug/comments
39. Add React front
40. Get Comments from an Article GET /api/articles/:slug/comments
41. Add React front
42. Delete Comment DELETE /api/articles/:slug/comments/:id
43. Add React front
44. Favorite Article POST /api/articles/:slug/favorite
45. Add React front
46. Unfavorite Article DELETE /api/articles/:slug/favorite
47. Add React front
48. Get Tags GET /api/tags
49. Add React front

# Stop seeking for new staff not included in this plan.

## Especially DO NOT SEEK FOR THE FOLLOWING.

- ELM
- Clojure
- Haskell
- C
- Rust
- Go
- Racket

# #100DaysOfCode

---

[1/100] 2019-08-24

Setup Django and documentation.

https://gitlab.com/kentaro0919/100daysofcode  
#100DaysOfCode

---

[2/100] 2019-08-25  
Setup Docker and Deploy to Heroku
As many blogs have old information, grad to see that official Heroku and Docker document works.

https://gitlab.com/kentaro0919/100daysofcode  
https://hundred-days-api-001.herokuapp.com/  
#100DaysOfCode# #100DaysOfCode

---

[3/100] 2019-08-26  
Working on Heroku but not working.

https://gitlab.com/kentaro0919/100daysofcode  
#100DaysOfCode

---

[4/100] 2019-08-27
Working on Heroku but not working.

https://gitlab.com/kentaro0919/100daysofcode  
https://hundred-days-api-001.herokuapp.com/  
#100DaysOfCode

---

[5/100] 2019-08-28
Working on Heroku but not working.

https://gitlab.com/kentaro0919/100daysofcode  
https://hundred-days-api-001.herokuapp.com/  
#100DaysOfCode

---

[6/100] 2019-08-29
Working on Heroku. WORKING!!

https://gitlab.com/kentaro0919/100daysofcode  
https://hundred-days-api-001.herokuapp.com/  
#100DaysOfCode

---

[7/100] 2019-08-30
Try to make local test for custom User with local setting.
but no able to run tests.
"ValueError: Dependency on app with no migrations: users"
Not sure how to run this...

https://gitlab.com/kentaro0919/100daysofcode  
https://hundred-days-api-001.herokuapp.com/  
#100DaysOfCode

---

[8/100] 2019-08-31
Fix setting of Docker and test
push -> test on ci -> deploy
is running as expected!!
change user model and add tag model

https://gitlab.com/kentaro0919/100daysofcode  
https://hundred-days-api-001.herokuapp.com/  
#100DaysOfCode

---

[9/100] 2019-09-01
done with basic CRUD
try to add DRF but stop running the test.
manage to remove and come back with the test

https://gitlab.com/kentaro0919/100daysofcode  
https://hundred-days-api-001.herokuapp.com/  
#100DaysOfCode

---

[10/100] 2019-09-02
The problem was not on DRF but with heroku.
adding django-heroku fix it.
DRF is working now

https://gitlab.com/kentaro0919/100daysofcode  
https://hundred-days-api-001.herokuapp.com/  
#100DaysOfCode

---
